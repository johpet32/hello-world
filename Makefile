CC=gcc
CFLAGS=-Wall -Wextra -Wformat=2 -Wconversion -Wuninitialized -O2

all: hello

hello: hello.c
	$(CC) $(CFLAGS) -o hello hello.c

clean:
	rm -f hello

distclean: clean
