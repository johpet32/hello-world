#include <stdio.h>
#include "hello.h"

void print_hello(int caps) {
	if (caps)
		puts("HELLO WORLD!");
	else
		puts("Hello World!");
}

int main(void) {
	print_hello(0);
	print_hello(1);

	return 0;
}
